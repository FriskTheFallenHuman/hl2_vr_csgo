import os
import sys
import subprocess
import argparse
import hashlib


SCRIPT_PATH = os.path.split(os.path.abspath(__file__))[0]


def read_line(string: str) -> list:
    line = []
    quote = ''
    in_quote = False
    char_num = 0
    while char_num < len(string):
        char = string[char_num]
        
        if char == '"':
            if in_quote:
                line.append(quote)
                quote = ""
                
            in_quote = not in_quote
        elif in_quote:
            quote += char
            
        char_num += 1
    
    return line


# Source: https://bitbucket.org/prologic/tools/src/tip/md5sum
def make_hash(filename: str) -> str:
    md5 = hashlib.md5()
    if os.path.isfile(filename):
        with open(filename, "rb") as f:
            for chunk in iter(lambda: f.read(128 * md5.block_size), b""):
                md5.update(chunk)
        return md5.hexdigest()
    else:
        return ""
    
    
def get_hash_dict(hash_path: str) -> dict:
    hash_dict = {}
    
    if not os.path.isfile(hash_path):
        return hash_dict
    
    with open(hash_path, "r") as hash_io:
        hash_text = hash_io.read()
        for line in hash_text.split("\n"):
            if not line:
                continue
            # file_hash, file_path = line.split(" ", 1)
            hash_dict[file_hash] = file_path
            
    return hash_dict


def get_hash_list(hash_path: str) -> list:
    hash_list = []
    
    if not os.path.isfile(hash_path):
        return hash_list
    
    with open(hash_path, "r") as hash_io:
        hash_text = hash_io.read()
        for line in hash_text.split("\n"):
            if not line:
                continue
            hash_list.append(read_line(line))
            
    return hash_list


def check_hash(hash_path: str, in_file: str, out_file: str) -> bool:
    if not os.path.isfile(hash_path):
        return False
        
    # hash_dict = get_hash_dict(hash_path)
    hash_list = get_hash_list(hash_path)
    
    file_hash = make_hash(in_file)
    
    for line in hash_list:
        if file_hash in line and out_file in line:
            return True
    return False
        
        
def update_hash(hash_path: str, in_file: str, out_file: str):
    hash_list = get_hash_list(hash_path)

    file_hash = make_hash(in_file)
    
    for line in hash_list:
        if file_hash in line and out_file in line:
            return

    # hash_file = [f"\"{tmp_hash}\" \"{tmp_in}\" \"{tmp_out}\"" for k, v in hash_dict.items()]
    hash_file = ['"' + '" "'.join(hash_list[index]) + '"' for index, _ in enumerate(hash_list)]
    hash_file.append(f"\"{file_hash}\" \"{in_file}\" \"{out_file}\"")
    
    with open(hash_path, "w") as hash_io:
        hash_io.write("\n".join(hash_file))


def update_hash_dict(hash_path: str, file: str):
    hash_dict = get_hash_dict(hash_path)

    file_hash = make_hash(file)
    if file_hash in hash_dict:
        return

    hash_file = [f"{k} {v}" for k, v in hash_dict.items()]
    hash_file.append(f"{file_hash} {file}")
    
    with open(hash_path, "w") as hash_io:
        hash_io.write("\n".join(hash_file))





