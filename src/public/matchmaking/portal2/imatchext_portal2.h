//===== Copyright c 1996-2009, Valve Corporation, All rights reserved. ======//
//
// Purpose: 
//
// $NoKeywords: $
//===========================================================================//

#ifndef IMATCHEXT_PORTAL2_H
#define IMATCHEXT_PORTAL2_H

#ifdef _WIN32
#pragma once
#pragma warning( push )
#pragma warning( disable : 4201 )
#endif

#define STORAGE_COUNT_FOR_BITS( aStorageType, numBits ) ( ( (numBits) + 8*sizeof( aStorageType ) - 1 ) / ( 8* sizeof( aStorageType ) ) )


//
//
//	WARNING!! WARNING!! WARNING!! WARNING!!
//		This structure TitleData1 should remain
//		intact after we ship otherwise
//		users profiles will be busted.
//		You are allowed to add fields at the end
//		as long as structure size stays under
//		XPROFILE_SETTING_MAX_SIZE = 1000 bytes.
//	WARNING!! WARNING!! WARNING!! WARNING!!
//



#define PORTAL2_LOBBY_CONFIG_COOP( szNetwork, szAccess ) \
		" system { " \
			" network " szNetwork " " \
			" access " szAccess " " \
		" } " \
		" game { " \
			" mode coop " \
			" map default " \
		" } "


#define PORTAL2_DLCID_RETAIL_DLC1			( 1ull << 0x01 )
#define PORTAL2_DLCID_RETAIL_DLC2			( 1ull << 0x02 )
#define PORTAL2_DLCID_COOP_BOT_SKINS		( 1ull << 0x12 )
#define PORTAL2_DLCID_COOP_BOT_HELMETS		( 1ull << 0x13 )
#define PORTAL2_DLCID_COOP_BOT_ANTENNA		( 1ull << 0x14 )
#define PORTAL2_DLC_ALLMASK ( PORTAL2_DLCID_RETAIL_DLC1 | PORTAL2_DLCID_RETAIL_DLC2 | PORTAL2_DLCID_COOP_BOT_SKINS | PORTAL2_DLCID_COOP_BOT_HELMETS | PORTAL2_DLCID_COOP_BOT_ANTENNA )

#define PORTAL2_DLC_APPID_COOP_BOT_SKINS		651
#define PORTAL2_DLC_APPID_COOP_BOT_HELMETS		652
#define PORTAL2_DLC_APPID_COOP_BOT_ANTENNA		653

#define PORTAL2_DLC_PKGID_COOP_BOT_SKINS		7364
#define PORTAL2_DLC_PKGID_COOP_BOT_HELMETS		7365
#define PORTAL2_DLC_PKGID_COOP_BOT_ANTENNA		7366

#define PORTAL2_DLC_PKGID_PCSTEAMPLAY			7397


#ifdef _WIN32
#pragma warning( pop )
#endif
#endif // IMATCHEXT_PORTAL2_H
