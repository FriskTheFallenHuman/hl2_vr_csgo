// ---------------------------------------------------------------
// mathlib.qpc
// ---------------------------------------------------------------
macro PROJECT_NAME "mathlib"

macro SRC_DIR ".."

include "$SRC_DIR/_qpc_scripts/source_lib_base.qpc"

configuration
{
	general
	{
		include_directories
		{
			"../public/mathlib"
		}
	}
	
	compiler
	{
		preprocessor_definitions
		{
			"MATHLIB_LIB"
		}
	}
}

files
{
	folder "Source Files"
	{
		"expressioncalculator.cpp"
		"color_conversion.cpp"
		"cholesky.cpp"
		"halton.cpp"
		"lightdesc.cpp"
		"mathlib_base.cpp"
		"powsse.cpp"
		"sparse_convolution_noise.cpp"
		"sseconst.cpp"
		"sse.cpp"
		"ssenoise.cpp"
		"anorms.cpp"
		"bumpvects.cpp"
		"IceKey.cpp"
		"kdop.cpp"
		"imagequant.cpp"
		"spherical.cpp"
		"polyhedron.cpp"
		"quantize.cpp"
		"randsse.cpp"
		"simdvectormatrix.cpp"
		"vmatrix.cpp"
		"almostequal.cpp"
		"simplex.cpp"
		"eigen.cpp"
		"box_buoyancy.cpp" [!$OSX32]
		"camera.cpp"
		"planefit.cpp"
		"polygon.cpp"
		"volumeculler.cpp"
		"transform.cpp"
		"sphere.cpp"
		"capsule.cpp"
	}

	folder "Public Header Files"
	{
		"$SRC_DIR/public/mathlib/anorms.h"
		"$SRC_DIR/public/mathlib/bumpvects.h"
		"$SRC_DIR/public/mathlib/beziercurve.h"
		"$SRC_DIR/public/mathlib/camera.h"
		"$SRC_DIR/public/mathlib/compressed_3d_unitvec.h"
		"$SRC_DIR/public/mathlib/compressed_light_cube.h"
		"$SRC_DIR/public/mathlib/compressed_vector.h"
		"$SRC_DIR/public/mathlib/expressioncalculator.h"
		"$SRC_DIR/public/mathlib/halton.h"
		"$SRC_DIR/public/mathlib/IceKey.H"
		"$SRC_DIR/public/mathlib/lightdesc.h"
		"$SRC_DIR/public/mathlib/math_pfns.h"
		"$SRC_DIR/public/mathlib/mathlib.h"
		"$SRC_DIR/public/mathlib/noise.h"
		"$SRC_DIR/public/mathlib/polyhedron.h"
		"$SRC_DIR/public/mathlib/quantize.h"
		"$SRC_DIR/public/mathlib/simdvectormatrix.h"
		"$SRC_DIR/public/mathlib/spherical_geometry.h"
		"$SRC_DIR/public/mathlib/ssemath.h"
		"$SRC_DIR/public/mathlib/ssequaternion.h"
		"$SRC_DIR/public/mathlib/vector.h"
		"$SRC_DIR/public/mathlib/vector2d.h"
		"$SRC_DIR/public/mathlib/vector4d.h"
		"$SRC_DIR/public/mathlib/vmatrix.h"
		"$SRC_DIR/public/mathlib/vplane.h"
		"$SRC_DIR/public/mathlib/simplex.h"
		"$SRC_DIR/public/mathlib/eigen.h"
		"$SRC_DIR/public/mathlib/box_buoyancy.h"
		"$SRC_DIR/public/mathlib/cholesky.h"
		"$SRC_DIR/public/mathlib/planefit.h"
		"$SRC_DIR/public/mathlib/intvector3d.h"
		"$SRC_DIR/public/mathlib/polygon.h"
		"$SRC_DIR/public/mathlib/quadric.h"
		"$SRC_DIR/public/mathlib/volumeculler.h"
		"$SRC_DIR/public/mathlib/transform.h"
		"$SRC_DIR/public/mathlib/sphere.h"
		"$SRC_DIR/public/mathlib/capsule.h"
	}

	folder "Header Files"
	{
		"noisedata.h"
		"sse.h"
	}
}
