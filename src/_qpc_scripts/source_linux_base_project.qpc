// ---------------------------------------------------------------
// source_linux_base_project.qpc
// ---------------------------------------------------------------

macro _WRAP_ "-Xlinker --wrap="

config
{
	general
	{
		compiler "g++-4.6"
	}
	
	compile
	{
		defines
		{
			"LINUX" "_LINUX" [$LINUX$]
			"_OSX" "OSX" "_DARWIN_UNLIMITED_SELECT" "FD_SETSIZE=10240" [$MACOS$]
			"_DEMO" [$DEMO$]
			
			"VPROF_LEVEL=1" "GNUC"
			
			// This causes all filesystem interfaces to default to their 64bit versions on
			// 32bit systems, which means we don't break on filesystems with inodes > 32bit.
			"_FILE_OFFSET_BITS=64"
		}
		
		// some of this probably needs to be moved to posix base
		options
		{
			"-O2" "-fno-strict-aliasing" "-ffast-math" "-fno-omit-frame-pointer"			[$RELEASE$]
			"-O0"																			[$DEBUG$]
			
			// nocona = pentium4 + 64bit + MMX, SSE, SSE2, SSE3 - no SSSE3 (that's three s's - added in core2)
			"-march=nocona"					[$LINUX64$]
			"-march=core2" "-m32"			[$LINUX32$]
			// could be -march=prescott? idk
			"-mtune=core2"
			
			"-MD" "-MP" "-MF"
			
			
			// Warning Flags
			"-Wall" "-Wextra" "-Wshadow" "-Wno-invalid-offsetof"	[$STEAM_BRANCH$]
			"-Wno-write-strings" "-Wno-multichar"					[!$STEAM_BRANCH$]
			
			// All Warning Flags
			"-Wno-unknown-pragmas"
			"-Wno-unused-parameter"
			"-Wno-unused-value"
			"-Wno-missing-field-initializers"
			"-Wno-sign-compare"
			"-Wno-reorder"
			"-Wno-invalid-offsetof"
			"-Wno-float-equal"
			"-Wno-switch"
			"-fdiagnostics-show-option"
			"-Wformat"
			"-Werror=format-security"
			"-Wstrict-aliasing=2"
			
			"-msse2"
			"-mfpmath=sse"		[!$CLANG]	// always false right now
			
			// The C-linkage return-type warning (no returning of references) must be disabled after the
			// return-type error is enabled.
			"-Wno-return-type-c-linkage" "-g0" "-Qunused-arguments"		[$CLANG$]
			
			// what is this
			"-fvisibility=hidden"
			
			"-std=gnu++0x" "-fpermissive"							[$LINUX$]
			"-std=gnu++11" "-stdlib=libc++" "-Wno-c++11-narrowing" "-Wno-dangling-else"	[!$LINUX$]

			"-ffast-math"
			// "-pipe"
			"-Usprintf"
			"-Ustrncpy"
			"-UPROTECTED_THINGS_ENABLE"
		}
	}
	
	link
	{
		libs
		{
			// "$SRC_DIR/thirdparty/gperftools-2.0/.libs/tcmalloc_minimal"			[$LINUX32 && !$DEDICATED]
			// "$SRC_DIR/thirdparty/gperftools-2.0/.libs/x86_64/tcmalloc_minimal"	[$LINUX64 && !$DEDICATED]
		}
		
		options
		{
			// All Warning Flags
			"-fdiagnostics-show-option"
			
			"-msse2"
			"-mfpmath=sse"		[!$CLANG$]	// always false right now
			
			// The C-linkage return-type warning (no returning of references) must be disabled after the
			// return-type error is enabled.
			"-g0" "-Qunused-arguments"		[$CLANG$]
			
			// what is this
			"-fvisibility=hidden"
			
			"-ffast-math"
			// "-pipe"
			"-Usprintf"
			"-Ustrncpy"
			"-UPROTECTED_THINGS_ENABLE"
			

			// maybe macos only?
			// please kill me
			"$_WRAP_$fopen" "$_WRAP_$freopen" "$_WRAP_$open"    "$_WRAP_$creat"    "$_WRAP_$access"  "$_WRAP_$__xstat"
			"$_WRAP_$stat"  "$_WRAP_$lstat"   "$_WRAP_$fopen64" "$_WRAP_$open64"   "$_WRAP_$opendir" "$_WRAP_$__lxstat"
			"$_WRAP_$chmod" "$_WRAP_$chown"   "$_WRAP_$lchown"  "$_WRAP_$symlink"  "$_WRAP_$link"    "$_WRAP_$__lxstat64"
			"$_WRAP_$mknod" "$_WRAP_$utimes"  "$_WRAP_$unlink"  "$_WRAP_$rename"   "$_WRAP_$utime"   "$_WRAP_$__xstat64"
			"$_WRAP_$mount" "$_WRAP_$mkfifo"  "$_WRAP_$mkdir"   "$_WRAP_$rmdir"    "$_WRAP_$scandir" "$_WRAP_$realpath"
		}
	}
}

