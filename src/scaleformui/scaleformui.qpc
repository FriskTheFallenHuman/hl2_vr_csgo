// ---------------------------------------------------------------
// scaleformui.qpc
// ---------------------------------------------------------------
macro PROJECT_NAME "ScaleformUI"

macro SRC_DIR ".."
macro OUT_BIN_DIR "$SRC_DIR/../game/bin"

include "$SRC_DIR/_qpc_scripts/source_dll_base.qpc"

macro GFXCONFIG_OSX "Shipping"
macro GFXBUILDSHIPPING_OSX "1"
macro GFXCONFIG_LINUX "shipping"
macro GFXBUILDSHIPPING_LINUX "1"
macro ENABLE_AMP_SERVER "1" [$WINDOWS]
macro ENABLE_AMP_SERVER "0" [!$WINDOWS]
macro WINPLATDIR "win32" [$WIN32]
macro WINPLATDIR "x64" [$WIN64]
macro GFXLIB "$SRC_DIR/thirdparty/scaleform/sdk42/Lib/$WINPLATDIR/msvc10" [$WINDOWS]
macro GFXLIB "$SRC_DIR/thirdparty/scaleform/sdk42/Lib/MacOS-i686" [$OSX32]
macro GFXLIB "$SRC_DIR/thirdparty/scaleform/sdk42/Lib/MacOS-x86_64" [$MACOS]
macro GFXLIB "$SRC_DIR/thirdparty/scaleform/sdk42/Lib/i386-linux" [$LINUX32]
macro GFXLIB "$SRC_DIR/thirdparty/scaleform/sdk42/Lib/x86_64-linux" [$LINUX64]
macro LIBJPEG "$SRC_DIR/thirdparty/scaleform/sdk42/3rdParty/jpeg-8d/lib/$WINPLATDIR/msvc10" [$WINDOWS && !$VS2015]
macro LIBJPEG "$SRC_DIR/thirdparty/scaleform/sdk42/3rdParty/jpeg-8d/lib/$WINPLATDIR/msvc15" [$WINDOWS && $VS2015]
macro LIBPNG "$SRC_DIR/thirdparty/scaleform/sdk42/3rdParty/libpng-1.5.13/Lib/$WINPLATDIR/msvc10" [$WINDOWS && !$VS2015]
macro LIBPNG "$SRC_DIR/thirdparty/scaleform/sdk42/3rdParty/libpng-1.5.13/Lib/$WINPLATDIR/msvc15" [$WINDOWS && $VS2015]
macro ZLIB "$SRC_DIR/thirdparty/scaleform/sdk42/3rdParty/zlib-1.2.7/Lib/$WINPLATDIR/msvc10" [$WINDOWS]

configuration
{
	general
	{
		include_directories
		{
			"$SRC_DIR/dx9sdk/Include" [$WINDOWS && !$GL]
			"$SRC_DIR/common/gl" [$GL]
			"$SRC_DIR/scaleformui"
			"$SRC_DIR/thirdparty/scaleform/sdk42/Include"
			"$SRC_DIR/thirdparty/scaleform/sdk42/Src"
			"$SRC_DIR/thirdparty"
		}
		
		library_directories
		{
			"$GFXLIB" [$PS3 || $MACOS || $LINUX]
			"$GFXLIB/debug" "$LIBJPEG/debug" "$LIBPNG/debug" "$ZLIB/debug" [$WINDOWS && $DEBUG]
			"$GFXLIB/release" [$ENABLE_AMP_SERVER && $WINDOWS && $RELEASE]
			"$GFXLIB/shipping" [!$ENABLE_AMP_SERVER && $WINDOWS && $RELEASE]
			"$LIBJPEG/release" "$LIBPNG/release" "$ZLIB/release" [$WINDOWS && $RELEASE]
			"$GFXLIB/$GFXCONFIG_OSX" [$MACOS]
		}
	}
	
	compiler
	{
		preprocessor_definitions
		{
			"USE_ACTUAL_DX" [($WINDOWS || $X360) && !$GL]
			"strncpy=use_Q_strncpy_instead" "_snprintf=use_Q_snprintf_instead" [!$POSIX]
			"GL_GLEXT_PROTOTYPES" "DX_TO_GL_ABSTRACTION" [$GL]
			"ALLOW_TEXT_MODE=1" [$CSTRIKE_TRUNK_BUILD || $CSTRIKE_STAGING_BUILD]
			"SF_BUILD_SHIPPING" [($LINUX && $GFXBUILDSHIPPING_LINUX) || ($MACOS && $GFXBUILDSHIPPING_OSX) && (!$ENABLE_AMP_SERVER && $WINDOWS && $RELEASE)]
			"SF_BUILD_DEBUG" [($LINUX && !$GFXBUILDSHIPPING_LINUX) || ($MACOS && !$GFXBUILDSHIPPING_OSX) && $DEBUG]
			"SF_BUILD_RELEASE" [($LINUX && !$GFXBUILDSHIPPING_LINUX) || ($MACOS && !$GFXBUILDSHIPPING_OSX) && $RELEASE]
			"INCLUDE_SCALEFORM"
			"ENABLE_AMP_SERVER=$ENABLE_AMP_SERVER"
			"VERSION_SAFE_STEAM_API_INTERFACES"
		}
		
		options
		{
			"/EHsc"
		}
	}
	
	linker
	{
		libraries
		{
			"winmm.lib" "libgfx.lib" "libgfx_as2.lib" "libgfx_as3.lib" "libgfxexpat.lib" "libgfx_ime.lib" "libgfxvideo.lib" "libjpeg.lib" "zlib.lib" "libpng.lib" "Ws2_32.lib" "imm32.lib" "oleaut32.lib" [$WINDOWS]
			"libgfxplatform_d3d9.lib" "libgfxrender_d3d9.lib" "$SRC_DIR/dx9sdk/lib$PLAT_DIR/d3d9" "$SRC_DIR/dx9sdk/lib$PLAT_DIR/d3dx9" "$SRC_DIR/dx9sdk/lib$PLAT_DIR/dxguid" [$WINDOWS && !$GL]
			"OpenGL32.lib" "libgfxplatform_GL.lib" "libgfxrender_GL.lib" [$GL]
			"legacy_stdio_definitions.lib" [$WINDOWS && $VS2015]
			"GL" [$LINUX]
			"Carbon" "OpenGL" "Quartz" "Cocoa" "IOKit" "iconv" "z" "$GFXLIB/$GFXCONFIG_OSX/libgfx.a" "$GFXLIB/libjpeg.a" "$GFXLIB/libpng.a" [$MACOS]
			"$GFXLIB/$GFXCONFIG_LINUX/libgfx" "$GFXLIB/$GFXCONFIG_LINUX/libgfxplatform" "$GFXLIB/$GFXCONFIG_LINUX/libgfx_as2" "$GFXLIB/$GFXCONFIG_LINUX/libgfxexpat" "$GFXLIB/$GFXCONFIG_LINUX/libgfxrender_gl" "$GFXLIB/libz" [$LINUX && !$DEDICATED]
			"$GFXLIB/$GFXCONFIG_OSX/libgfx" "$GFXLIB/$GFXCONFIG_OSX/libgfxplatform" "$GFXLIB/$GFXCONFIG_OSX/libgfx_as2" "$GFXLIB/$GFXCONFIG_OSX/libgfxrender_gl" "videocfg" [$MACOS]
			"$GFXLIB/libjpeg" "$GFXLIB/libpng" [($MACOS || $LINUX) && !$DEDICATED]
			"steam_api" [($WIN32 || $POSIX || $PS3) && !$NO_STEAM]
			"steam_api64" [$WIN64 && !$NO_STEAM]
			"togl" [!$IS_LIB_PROJECT && $GL && !$DEDICATED]
			"mathlib"
			"bitmap"
			"tier2"
			"tier3"
			"vtf"
		}
	}
}

dependencies
{
	"mathlib"
	"bitmap"
	"tier2"
	"tier3"
	"vtf"
	"videocfg"	[$MACOS]
	"togl"	[!$IS_LIB_PROJECT && $GL && !$DEDICATED]
}

files
{
	folder "Public Header Files"
	{
	}

	folder "Scaleform" [!$DEDICATED]
	{
		folder "Public Header Files"
		{
			"$SRC_DIR/public/scaleformui/scaleformui.h"
			"$SRC_DIR/public/tier1/interface.h"
		}

		folder "Precompiled Header Files"
		{
			"stdafx.h"
			"stdafx.cpp"
		}

		folder "ScaleformUIImpl"
		{
			"scaleformuiimpl/sfuimemoryfile.h"
			"scaleformuiimpl/movieslot.h"
			"scaleformuiimpl/movieslot.cpp"
			"scaleformuiimpl/scaleformuiimpl.h"
			"scaleformuiimpl/scaleformuicursorimpl.cpp"
			"scaleformuiimpl/scaleformuihighlevelimpl.cpp"
			"scaleformuiimpl/scaleformuiinitimpl.cpp"
			"scaleformuiimpl/scaleformuimovieimpl.cpp"
			"scaleformuiimpl/scaleformuimovieslotimpl.cpp"
			"scaleformuiimpl/scaleformuiparamsimpl.cpp"
			"scaleformuiimpl/scaleformuirendererimpl.cpp"
			"scaleformuiimpl/scaleformuitranslationimpl.cpp"
			"scaleformuiimpl/scaleformuivalueimpl.cpp"
			"scaleformuiimpl/scaleformuiintegration.cpp"
			"scaleformuiimpl/scaleformuitextobjimpl.cpp"
			"scaleformuiimpl/scaleformuiintegration.h"
			"scaleformuiimpl/scaleformuiimage.cpp"
			"scaleformuiimpl/scaleformuiimage.h"
			"scaleformuiimpl/sfuiavatarimage.cpp"
			"scaleformuiimpl/sfuiavatarimage.h"
			"scaleformuiimpl/sfuichromehtmlimage.cpp"
			"scaleformuiimpl/sfuichromehtmlimage.h"
			"scaleformuiimpl/sfuiinventoryimage.cpp"
			"scaleformuiimpl/sfuiinventoryimage.h"
			"$SRC_DIR/game/shared/cstrike15/dlchelper.h"
		}
	}
}
