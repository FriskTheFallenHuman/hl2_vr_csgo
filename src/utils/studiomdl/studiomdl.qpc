// ---------------------------------------------------------------
// studiomdl.qpc
// ---------------------------------------------------------------
macro PROJECT_NAME "Studiomdl"

macro SRC_DIR "../.."
macro OUT_BIN_DIR "$SRC_DIR/../game/bin"

include "$SRC_DIR/_qpc_scripts/source_exe_con_base.qpc"
include "$SRC_DIR/_qpc_scripts/fbx.qpc"

configuration
{
	general
	{
		include_directories
		{
			"../common"
			"../nvtristriplib"
			"$SRC_DIR/Game_Shared"
			"$SRC_DIR/ps3sdk/cell/host-common/include"
			"$SRC_DIR/ps3sdk/cell/target/common/include"
		}
		
		library_directories
		{
			"$SRC_DIR/ps3sdk/cell/host-win32/lib"
		}
	}
	
	compiler
	{
		preprocessor_definitions
		{
			"PROTECTED_THINGS_DISABLE"
		}
	}
	
	linker
	{
		libraries
		{
			"winmm.lib"
			"libedgegeomtool.Release.Win32.vs8.lib"
			"resourcefile"
			"dmeutils"
			"meshutils"
			"appframework"
			"bonesetup"
			"datamodel"
			"dmserializers"
			"mathlib"
			"mathlib_extended"
			"mdlobjects"
			"movieobjects"
			"nvtristrip"
			"tier1"
			"tier2"
			"tier3"
			"fbxutils"
		}
	}
}

dependencies
{
	"resourcefile"
	"appframework"
	"bonesetup"
	"datamodel"
	"dmserializers"
	"mathlib"
	"mathlib_extended"
	"mdlobjects"
	"movieobjects"
	"nvtristrip"
	"tier1"
	"tier2"
	"tier3"
}

files
{
	folder "Source Files"
	{
		"../common/cmdlib.cpp"
		"../common/datalinker.cpp"
		"collisionmodel.cpp"
		"collisionmodelsource.cpp"
		"$SRC_DIR/public/collisionutils.cpp"
		"dmxsupport.cpp"
		"$SRC_DIR/public/filesystem_helpers.cpp"
		"$SRC_DIR/public/filesystem_init.cpp"
		"../common/filesystem_tools.cpp"
		"hardwarematrixstate.cpp"
		"hardwarevertexcache.cpp"
		"$SRC_DIR/public/interpolatortypes.cpp"
		"$SRC_DIR/public/mdlobjects/mdlobjects.cpp"
		"$SRC_DIR/public/movieobjects/movieobjects_compiletools.cpp"
		"mrmsupport.cpp"
		"objsupport.cpp"
		"optimize_subd.cpp"
		"optimize.cpp"
		"perfstats.cpp"
		"../common/physdll.cpp"
		"../common/scriplib.cpp"
		"simplify.cpp"
		"$SRC_DIR/public/studio.cpp"
		"$SRC_DIR/common/studiobyteswap.cpp"
		"studiomdl.cpp"
		"compileclothproxy.cpp"
		"UnifyLODs.cpp"
		"v1support.cpp"
		"write.cpp"
	}

	folder "Header Files"
	{
		"../common/cmdlib.h"
		"../common/datalinker.h"
		"collisionmodel.h"
		"collisionmodelsource.h"
		"physics2collision.h"
		"physics2collision.h"
		"filebuffer.h"
		"../common/filesystem_tools.h"
		"hardwarematrixstate.h"
		"hardwarevertexcache.h"
		"../nvtristriplib/nvtristrip.h"
		"perfstats.h"
		"../common/physdll.h"
		"../common/scriplib.h"
		"studiomdl.h"
		"optimize_subd.h"
		"compileclothproxy.h"
	}

	folder "Public Header Files"
	{
		"$SRC_DIR/public/alignedarray.h"
		"$SRC_DIR/public/gametrace.h"
		"$SRC_DIR/public/filesystem.h"
		"$SRC_DIR/public/filesystem_helpers.h"
		"$SRC_DIR/public/cmodel.h"
		"$SRC_DIR/public/basehandle.h"
		"$SRC_DIR/public/tier0/basetypes.h"
		"$SRC_DIR/public/bitvec.h"
		"$SRC_DIR/public/bone_accessor.h"
		"$SRC_DIR/public/bone_setup.h"
		"$SRC_DIR/public/bspflags.h"
		"$SRC_DIR/public/tier1/byteswap.h"
		"$SRC_DIR/public/tier1/characterset.h"
		"$SRC_DIR/public/collisionutils.h"
		"$SRC_DIR/public/mathlib/compressed_vector.h"
		"$SRC_DIR/public/const.h"
		"$SRC_DIR/public/vphysics/constraints.h"
		"$SRC_DIR/public/tier0/dbg.h"
		"$SRC_DIR/public/tier0/fasttimer.h"
		"$SRC_DIR/public/appframework/iappsystem.h"
		"$SRC_DIR/public/tier0/icommandline.h"
		"$SRC_DIR/public/ihandleentity.h"
		"$SRC_DIR/public/materialsystem/imaterial.h"
		"$SRC_DIR/public/materialsystem/imaterialsystem.h"
		"$SRC_DIR/public/materialsystem/imaterialvar.h"
		"$SRC_DIR/public/tier1/interface.h"
		"$SRC_DIR/public/istudiorender.h"
		"$SRC_DIR/public/tier1/keyvalues.h"
		"$SRC_DIR/public/materialsystem/materialsystem_config.h"
		"$SRC_DIR/public/mathlib/mathlib.h"
		"$SRC_DIR/public/tier0/memdbgoff.h"
		"$SRC_DIR/public/tier0/memdbgon.h"
		"$SRC_DIR/public/phyfile.h"
		"$SRC_DIR/public/phzfile.h"
		"$SRC_DIR/public/optimize.h"
		"$SRC_DIR/public/tier0/platform.h"
		"$SRC_DIR/public/vstdlib/random.h"
		"$SRC_DIR/common/studiobyteswap.h"
		"$SRC_DIR/public/string_t.h"
		"$SRC_DIR/public/tier1/strtools.h"
		"$SRC_DIR/public/studio.h"
		"$SRC_DIR/public/tier3/tier3.h"
		"$SRC_DIR/public/tier1/utlbuffer.h"
		"$SRC_DIR/public/tier1/utldict.h"
		"$SRC_DIR/public/tier1/utllinkedlist.h"
		"$SRC_DIR/public/tier1/utlmemory.h"
		"$SRC_DIR/public/tier1/utlrbtree.h"
		"$SRC_DIR/public/tier1/utlsymbol.h"
		"$SRC_DIR/public/tier1/utlvector.h"
		"$SRC_DIR/public/vcollide.h"
		"$SRC_DIR/public/vcollide_parse.h"
		"$SRC_DIR/public/mathlib/vector.h"
		"$SRC_DIR/public/mathlib/vector2d.h"
		"$SRC_DIR/public/mathlib/vector4d.h"
		"$SRC_DIR/public/mathlib/vmatrix.h"
		"$SRC_DIR/public/vphysics_interface.h"
		"$SRC_DIR/public/mathlib/vplane.h"
		"$SRC_DIR/public/tier0/vprof.h"
		"$SRC_DIR/public/vstdlib/vstdlib.h"
	}
}
