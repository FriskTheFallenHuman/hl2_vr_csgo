// ---------------------------------------------------------------
// vpc.qpc
// ---------------------------------------------------------------
macro PROJECT_NAME "$VPCNAME"

macro VPCNAME "vpc" [$STATIC_LINK]
macro VPCNAME "vpcd" [!$STATIC_LINK]
macro VPCNAME "vpcs_test" [$VPC_STATIC_LINK_TEST]
macro SRC_DIR "../../.."
macro OUT_BIN_DIR "$SRC_DIR/devtools/bin" [$WIN32]
macro OUT_BIN_DIR "$SRC_DIR/devtools/bin/linux64" [$LINUX64]
macro OUT_BIN_DIR "$SRC_DIR/devtools/bin/osx" [$OSX32]
macro RPATH_TO_STANDARD_BINS "$ORIGIN/../../../../game/bin$PLAT_DIR"
macro LIBSUBDIR "$PLAT_DIR" [$STATIC_LINK && $WINDOWS]
macro OUT_BIN_NAME "$VPCNAME"
macro _STATICSUBDIR "Static" [$STATIC_LINK]
macro _STATICSUBDIR "StaticTest" [$VPC_STATIC_LINK_TEST]
macro DISABLE_TCMALLOC "1" [$LINUX]
macro REQUIRES_ITERATOR_DEBUGGING "true" [$LINK_P4API]
macro DISABLE_MULTIPROCESSOR_COMPILATION "true"

include "$SRC_DIR/_qpc_scripts/source_dll_base.qpc" [!$STATIC_LINK]
include "$SRC_DIR/_qpc_scripts/source_exe_con_base.qpc" [$STATIC_LINK]

configuration
{
	general
	{
		include_directories
		{
			"../../../thirdparty/clang/include"
		}
	}
	
	compiler
	{
		preprocessor_definitions
		{
			"_USRDLL" [$WINDOWS]
			"_USE_32BIT_TIME_T" [$WIN32]
		}
	}
	
	linker
	{
		libraries
		{
			"ws2_32.lib" [$WINDOWS]
			"iconv" "Foundation" "CoreServices" [$MACOS]
			"rt" [$LINUX]
			"p4lib_staticlink" [$LINK_P4API]
			"$LIBCOMMON/libclient" "$LIBCOMMON/libp4sslstub" "$LIBCOMMON/librpc" "$LIBCOMMON/libsupp" [$LINK_P4API && !$VS2015]
			"$LIBCOMMON/2015/libclient" "$LIBCOMMON/2015/libp4sslstub" "$LIBCOMMON/2015/librpc" "$LIBCOMMON/2015/libsupp" [$LINK_P4API && $VS2015]
		}
	}
}

dependencies
{
	"binlaunch;p4lib;filesystem_stdio"	[!$STATIC_LINK]
}

files
{
	folder "Source Files"
	{
		"baseprojectdatacollector.cpp"
		"$SRC_DIR/common/clang/clang_utils.cpp"
		"clanggenerator.cpp"
		"conditionals.cpp"
		"configuration.cpp"
		"../vpccrccheck/crccheck_shared.cpp"
		"dependencies.cpp"
		"$SRC_DIR/common/environment_utils.cpp"
		"$SRC_DIR/common/bundled_module_info.cpp"
		"generatordefinition.cpp"
		"generated_files.cpp"
		"groupscript.cpp"
		"macros.cpp"
		"main.cpp"
		"p4sln.cpp" [$WINDOWS]
		"pch_helpers.cpp"
		"projectgenerator_android.cpp"
		"projectgenerator_vcproj.cpp"
		"projectgenerator_makefile.cpp"
		"projectgenerator_win32.cpp"
		"projectgenerator_win32_2010.cpp"
		"projectgenerator_ps3.cpp"
		"projectgenerator_xbox360.cpp"
		"projectgenerator_xbox360_2010.cpp"
		"projectscript.cpp"
		"qtgenerator.cpp"
		"schemagenerator.cpp"
		"scriptsource.cpp"
		"solutiongenerator_makefile.cpp"
		"solutiongenerator_xcode.cpp"
		"solutiongenerator_win32.cpp"
		"sys_utils.cpp"
		"unity.cpp"
		"updateautoexp.cpp" [$WINDOWS]
	}

	folder "Header Files"
	{
		"baseprojectdatacollector.h"
		"$SRC_DIR/common/clang/clang_utils.h"
		"dependencies.h"
		"$SRC_DIR/common/environment_utils.h"
		"generatordefinition.h"
		"ibaseprojectgenerator.h"
		"ibasesolutiongenerator.h"
		"p4sln.h" [$WINDOWS]
		"projectgenerator_android.h"
		"projectgenerator_android.inc"
		"projectgenerator_ps3.h"
		"projectgenerator_ps3.inc"
		"projectgenerator_win32_2010.h"
		"projectgenerator_win32_2010.inc"
		"projectgenerator_win32.h"
		"projectgenerator_win32.inc"
		"projectgenerator_vcproj.h"
		"projectgenerator_xbox360.h"
		"projectgenerator_xbox360.inc"
		"projectgenerator_xbox360_2010.h"
		"projectgenerator_xbox360_2010.inc"
		"projectgenerator_makefile.h"
		"scriptsource.h"
		"sys_utils.h"
		"vpc.h"
	}

	folder "VPC Definitions"
	{
		"$SRC_DIR/vpc_scripts/definitions/ps3.def"
		"$SRC_DIR/vpc_scripts/definitions/xbox360.def"
		"$SRC_DIR/vpc_scripts/definitions/xbox360_2010.def"
		"$SRC_DIR/vpc_scripts/definitions/win32_2005.def"
		"$SRC_DIR/vpc_scripts/definitions/win32_2010.def"
	}
}
